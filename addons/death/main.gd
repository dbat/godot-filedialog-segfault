@tool
extends MarginContainer

@onready var file_dialog: FileDialog = $FileDialog


# This fixes the crash. Remark to see the segfault.
func _exit_tree() -> void:
	remove_child($FileDialog)
