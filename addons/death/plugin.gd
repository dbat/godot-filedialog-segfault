@tool
extends EditorPlugin

const MainViewScene = preload("main.tscn")
const MainView = preload("main.gd")

var main_view: MainView
var rwbutton : Button

func _enter_tree() -> void:
	if Engine.is_editor_hint():
		main_view = MainViewScene.instantiate()
		rwbutton = add_control_to_bottom_panel(main_view, "Death")


func _exit_tree() -> void:
	remove_control_from_bottom_panel(main_view)
	main_view.queue_free()


func _has_main_screen() -> bool:
	return false


func _make_visible(next_visible: bool) -> void:
	if is_instance_valid(main_view):
		main_view.visible = next_visible


func _get_plugin_name() -> String:
	return "Death"

